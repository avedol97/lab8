package com.demo.springboot;

public class CheckPesel {

    private int pesel[] = new int[11];
    private String result = "false";

    public CheckPesel(String NumberOfPesel){
        if(NumberOfPesel.length() == 11 ){
            for(int i=0;i<11;i++){
                pesel[i]= Integer.parseInt(NumberOfPesel.substring(i,i+1));
            }
            if(check() == "true"){
                result="true";
            }

    }}

    private String check(){
        int sum = pesel[0] + 3 * pesel[1] + 7 * pesel[2] + 9 * pesel[3] + pesel[4] + 3 * pesel[5] +
                7 * pesel[6] + 9 * pesel[7] + pesel[8] + 3 * pesel[9];
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;

        if (sum == pesel[10]) {
            return "true";
        }
        else {
            return "false";
        }
    }

    public String getResult(){
        return result;
    }

}
